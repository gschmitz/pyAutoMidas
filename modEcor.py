
import os
import sys
import numpy
import string

# own modules
import modDecompInfo
import molutil

ang_to_bohr = 1.889725989

class Ecor:

	def __init__(self):

		self.IoLevel = 10
		self.Name = "Ecor Calculation"

		self.CCModel = "CC2"

		self.CPTensors = True
		self.RIMP2Algo = "Normal"

		self.xyz = numpy.zeros([0,3],dtype="float64")
		self.xyz_init = False
		self.elems = []
		self.natoms = 0
		self.nelect = 0

		self.DecompInfo = modDecompInfo.DecompInfo(3)
                self.decompinfo_init = False

                self.TolSVD = 1.e-12
                self.TolErr = 1.e-5

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for Ecor module"
			print ""
			print "     a <file>           : add coordinate file. "
			print "                          Supported XZY file, Turbomole coord file"
			print "     tolsvd   <double>  : sets threshold for the SVD in the RI-SVD step"
			print "     tolerr   <double>  : sets threshold for the maximal error in the C2T/T2C algorithm"
			print "     decomp             : call interactive input for tensor decomposer"
			print "     iolevel <int>      : set IoLevel for Ecor module"
			print "     rimp2   <string>   : select a RI-MP2 algorithm. Available:"
			print "                           [1] RIMP2"
			print "                           [2] THC-MP2"
			print "                           [2] THC-SOS-MP2"
			print "                           [2] AB-THC-SOS-MP2"
			print ""
			print "     ccmodel <string>   : specififes the CC model e.g. CC2"
			print "     name    <string>   : Give a name for this module"
			print "     cptensors <on/off> : use CP decomposed amplitudes?"
			print "     *                  : go to next menu"
			print "     q                  : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break
		
			elif sline.startswith("a "):
				token = sline.split()
				path = token[1]
				if os.path.isfile(path) == True:
					extension = os.path.splitext(path)[1]

					if extension == ".xyz":
						self.natoms, self.xyz, self.elems = molutil.read_xyz(path)
						self.xyz_init = True
			
					elif os.path.basename(path) == "coord":
						self.natoms, self.xyz, self.elems = molutil.read_coord(path)
						self.xyz_init = True

				print ""
				print " Added molecule with " + str(self.natoms) + " atoms"
				print ""			

			elif sline.startswith("tolsvd "):
				token = sline.split()
				self.TolSVD = float(token[1])

			elif sline.startswith("tolerr "):
				token = sline.split()
				self.TolErr = float(token[1])
			
			elif sline.startswith("decomp"):
				ilevel = 3 # Input Level for DecompInfo is 3
				objDecompInfo = modDecompInfo.DecompInfo(ilevel)
				objDecompInfo.GetInteractiveInput()
				self.SetDecompInfo(objDecompInfo)
				print ""
				print " Info on decomposer added "
				print ""			

			elif sline.startswith("iolevel "):
				token = sline.split()
				self.IoLevel = int(token[1])
				print ""
				print " IoLevel for Ecor is now: " + str(self.IoLevel)
				print ""			

			elif sline.startswith("name "):
				token = sline.split()
				self.Name = token[1]
				print ""
				print " Name for this Ecor module is now: " + str(self.Name)
				print ""			

			elif sline.startswith("ccmodel "):
				token = sline.split()
				if str.lower(token[1]) == "cc2":
					self.CCModel = "CC2"
				elif str.lower(token[1]) == "cc3":
					self.CCModel = "CC3"
				else: 	
					print " Unknown option for ccmodel! -> I will change nothing"
				print ""
				print " CCModel is now: " + str(self.CCModel)
				print ""		

			elif sline.startswith("cptensors "):
				token = sline.split()
				if str.lower(token[1]) == "on":
					self.CPTensors = True
				elif str.lower(token[1]) == "off":
					self.CPTensors = False
				else: 	
					print " Unknown option for cptensors! -> I will change nothing"
				print ""
				print " CPTensors is now: " + str(self.CPTensors)
				print ""		

			elif sline.startswith("rimp2 "):
				token = sline.split()
				if str.lower(token[1]) == "normal":
					self.RIMP2Algo = "Normal"
				elif str.lower(token[1]) == "batched":
					self.RIMP2Algo = "Batched"
				elif str.lower(token[1]) == "thc-mp2":
					self.RIMP2Algo = "THC-MP2"
				elif str.lower(token[1]) == "thc-sos-mp2":
					self.RIMP2Algo = "THC-SOS-MP2"
				elif str.lower(token[1]) == "ab-thc-sos-mp2":
					self.RIMP2Algo = "AB-THC-SOS-MP2"
				elif str.lower(token[1]) == "ab-thc-mp2":
					self.RIMP2Algo = "AB-THC-MP2"
				elif str.lower(token[1]) == "rimp2":
					self.RIMP2Algo = "RIMP2"
				else:
					print " Unknown option for rimp2! -> I will change nothing"
				print ""
				print " RI-MP2 algorithm is now: " + str(self.RIMP2Algo)
				print ""		

		return


	def SetDecompInfo(self,objDecompInfo):
		self.DecompInfo = objDecompInfo
		self.decompinfo_init = True

		return

	def GetDataGroup(self):

		MyString = "#1 Elec\n"

		if self.xyz_init == True:
			MyString += "#2 MOLSTRUCT\n"
			MyString += str(self.natoms) + " au\n"

			for i in xrange(self.natoms):
 				xyz = self.xyz[i]
				sline = "%-5s %12.6f %12.6f %12.6f" % tuple([self.elems[i]]+xyz.tolist())
				MyString += sline + "\n"

		MyString += "#2 Ecor\n"

		MyString += "#3 IoLevel\n"
		MyString += str(self.IoLevel) + "\n"

		MyString += "#3 Name\n"
		MyString += self.Name + "\n"

		MyString += "#3 CCModel\n"
		MyString += self.CCModel + "\n"

		MyString += "#3 ThrSVDRI\n"
		MyString += "{0:.2e}".format(self.TolSVD) + "\n"

		MyString += "#3 ThrErr\n"
		MyString += "{0:.2e}".format(self.TolErr) + "\n"

		if self.CPTensors == True:
			MyString += "#3 CPTensors\n"

		MyString += "#3 RIMP2\n"
		MyString += self.RIMP2Algo + "\n"

		if self.decompinfo_init == True:
			MyString += self.DecompInfo.GetDataGroup()

		return MyString



