#!/usr/bin/env python

import sys

import modDecompInfo
import modEcor
import modGeneral
import modSinglePoint
import modSystem

import pyAutoMidas

have_general = False

numsp = 1

# init Midas Input class
objMidasInp = pyAutoMidas.MidasInput()

# give defaul for output file
filinp = "Midas.inp"

print "*----------------------------------------------------------------------*"
print "*                             MIDAS DEFINE                             *"       
print "*                                                                      *"       
print "*       interactive program to setup the input file for Midas          *"       
print "*----------------------------------------------------------------------*"
print ""

while True:
	
	print "        General setup for Midas input"
	print ""
	print "     ouput    : Sets the name for the input file (Default: Midas.inp)" 
	print "     general  : Add general input options beyond the defaults "
	print "     sp       : Add a single point calculation to the input "
	print "     system   : Add System section to input file "
	print "     ecor     : Add information for electronic structure calculations"
	print "     *        : leave program and write input"
	print "     q        : quit progam (input will not be written)"
	print ""

	sline = raw_input("Command:  ").strip()
		
	if sline.startswith("q"): 
		sys.exit(0)
		
	elif sline.startswith("*"): 
		break

	elif sline.startswith("output"):
		token = sline.split()
		filinp = token[1]  # ToDo: check if filinp is valid filename	

	elif sline.startswith("general"):
		objGeneral = modGeneral.General()	# create object for general input
		objGeneral.GetInteractiveInput()		# setup input interatively by the user
		objMidasInp.SetGeneral(objGeneral)	# add general input to input file
		have_general = True						# logic to overwrite defaults
		
	elif sline.startswith("ecor"):
		objEcor = modEcor.Ecor()				# create ecor object
		objEcor.GetInteractiveInput()			# setup input interatively by the user
		objMidasInp.SetEcor(objEcor)			# add ecor module to input file

	elif sline.startswith("system"):
		objSystem = modSystem.System(objMidasInp.SinglePoints)				
		objSystem.GetInteractiveInput()			
		objMidasInp.SetSystem(objSystem)			

	elif sline.startswith("sp"):
		objSP = modSinglePoint.SinglePoint()
		objSP.Name += str(numsp)  
		objSP.GetInteractiveInput()
		objMidasInp.AddSinglePoint(objSP)
		numsp += 1

#end while

# for some modules we give default values if the user did not set the info
if have_general == False:
	objGeneral = modGeneral.General()	
	objMidasInp.SetGeneral(objGeneral)

# write input file to disk
objMidasInp.ToFile(filinp)

		
	
