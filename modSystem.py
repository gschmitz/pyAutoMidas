
import sys

import modSysDef
import modModSys

class System:

	def __init__(self,lstOfSinglePoints):

		self.IoLevel = 10
		self.init_iolevel = False

		self.Systems = []
		self.init_sys = False
		self.num_sys = 0

		self.SinglePoints = lstOfSinglePoints

		self.ModSys = modModSys.ModSys(2,self.Systems,lstOfSinglePoints)
		self.init_modsys = False

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for System input"
			print ""
			print "     iolevel <int>  : set local IoLevel"
			print "     sysdef         : add a system definition"
			print "     modsys         : specifiy how to modify the system information"
			print "     *              : go to next menu"
			print "     q              : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break
		
			elif sline.startswith("iolevel "):
				token = sline.split()
				self.IoLevel = int(token[1])
				self.init_iolevel = True
				print ""
				print " IoLevel is now: " + str(self.IoLevel)
				print ""			

			elif sline.startswith("sysdef"):
				ilevel = 2 # Input Level for SysDef
				objSys = modSysDef.SysDef(ilevel)
				objSys.GetInteractiveInput()
				self.AddSystem(objSys)

			elif sline.startswith("modsys"):
				ilevel = 2 # Input Level for ModSys
				objModSys = modModSys.ModSys(ilevel,
													  self.Systems,
													  self.SinglePoints)
				objModSys.GetInteractiveInput()
				self.SetModSys(objModSys)

		return

	def AddSystem(self,objSys):
		self.Systems.append(objSys)
		self.init_sys = True
		self.num_sys += 1
		return

	def SetModSys(self,objModSys):
		self.ModSys = objModSys
		self.init_modsys = True

	def GetDataGroup(self):

		MyString = "#1 System\n"

		if self.init_iolevel == True:		
			MyString += "#2 IoLevel\n"
			MyString += str(self.IoLevel) + "\n"

		if self.init_sys == True:
			for isys in xrange(self.num_sys):
				MyString += self.Systems[isys].GetDataGroup()

		if self.init_modsys == True:
			MyString += self.ModSys.GetDataGroup()

		return MyString



