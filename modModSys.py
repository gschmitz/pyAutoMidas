
import sys

import modFreqAna

class ModSys:

	def __init__(self,ilevel,lstOfSys,lstOfSinglePoints):

		self.InputLevel = ilevel
		
		self.Systems = lstOfSys

		self.RemoveGloablTrans = True

		self.SinglePoints = lstOfSinglePoints

		self.ModVibCoord = "Fregana"
		self.FreqAna = modFreqAna.FreqAna(ilevel+2,lstOfSinglePoints)

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for general input"
			print ""
			print "     vibcoord <string>  : How to handle the vibrational coordinate. Options:"
			print "                          ROTCOORD      : rotation of global vibrational coordinates accordint to a certain measure"
			print "                          ITERMERGESYS  : iterative merge of subsystems"
			print "                          FREQANA       : frequency analysis (Default)"
			print ""
			print "     *                  : go to next menu"
			print "     q                  : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break

			elif sline.startswith("vibcoord"):
				token = sline.split()
				if str.upper(token[1]) == "FREQANA":
					self.ModVibCoord = "Fregana"
					objFreqAna = modFreqAna.FreqAna(self.InputLevel + 2,self.SinglePoints)
					objFreqAna.GetInteractiveInput() 
					self.FreqAna = objFreqAna

				elif str.upper(token[1]) == "ITERMERGESYS":
					self.ModVibCoord = "Intemergesys"
				elif str.upper(token[1]) == "ROTCOORD":
					self.ModVibCoord = "Rotcoord"
		
		return

	def GetDataGroup(self):

		#shortcut
		ilvl = self.InputLevel

		MyString = "#" + str(ilvl) + " ModSys\n"

		if self.RemoveGloablTrans == True:
			MyString += "#" + str(ilvl+1) + " REMOVEGLOBALTR\n"

		MyString += "#" + str(ilvl+1) + " Sysnames\n"
		isys = 1
		for objSys in self.Systems:
			MyString += str(isys) + "\n"
			MyString += objSys.Name + "\n"

			isys += 1

		MyString += "#" + str(ilvl+1) + " ModVibCoord\n"
		if self.ModVibCoord == "Fregana":
			MyString += "Freqana\n"
			MyString += self.FreqAna.GetDataGroup()
		elif self.ModVibCoord == "Rotcoord":
			MyString += "Rotcoord\n"
			print "Option Rotcoord not yet supported"
			sys.exit(0)
		elif self.ModVibCoord == "Intermergesys":
			MyString += "Intemergesys\n"
			print "Option Intermergesys  not yet supported"
			sys.exit(0)

		return MyString



