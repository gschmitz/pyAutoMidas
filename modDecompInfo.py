import sys

class DecompInfo:

	def __init__(self,ilevel):
		self.InputLevel = ilevel

		self.Type = "FINDBESTCPALS"
		self.MaxIter = 200
		self.CPMaxRank = 500
		self.ThrCPRel = 1.e-1
		self.ThrFindCP = 1.e-3
		self.Guesser = "RANDOM"
		self.GuessInc = "FITDIFF"

		return

	def GetInteractiveInput(self):
		while True:
			print ""
			print "        Add info on tensor decomposer"
			print ""
			print "     type <string>     : sets type "
			print "     maxrank <int>     : sets maximum rank"
			print "     tolcp   <double>  : sets threshold for CP decomposition"
			print "     tolrel  <double>  : sets the relative CP-ALS threhsold"
			print "     *                 : go to next menu"
			print "     q                 : quit program (Input will not be written)"
			print ""

			sline = raw_input("Command:  ").strip()

			if sline.startswith("q"):
				sys.exit(0)

			elif sline.startswith("*"):
				break

			elif sline.startswith("maxrank "):
				token = sline.split()
				self.CPMaxRank = int(token[1])

			elif sline.startswith("tolcp "):
				token = sline.split()
				self.ThrFindCP = float(token[1])

			elif sline.startswith("tolrel "):
				token = sline.split()
				self.ThrCPRel = float(token[1])

		return

	def GetDataGroup(self):

		# shortcut
		ilvl = self.InputLevel

		MyString = "#" + str(ilvl) + " TENSORDECOMPINFO\n"

		if self.Type == "FINDBESTCPALS":
			MyString += "#" + str(ilvl + 1) + " TYPE\n"
			MyString += self.Type + "\n"
			
			MyString += "#" + str(ilvl + 1) + " CPALSRELATIVETHRESHOLD\n"
			MyString += "{0:.2e}".format(self.ThrCPRel) + "\n"

			MyString += "#" + str(ilvl + 1) + " CPALSMAXITER\n"
			MyString += str(self.MaxIter) + "\n"

			MyString += "#" + str(ilvl + 1) + " FINDBESTCPMAXRANK\n"
			MyString += str(self.CPMaxRank) + "\n"

			MyString += "#" + str(ilvl + 1) + " FINDBESTCPRESTHRESHOLD\n" 
			MyString += "{0:.2e}".format(self.ThrFindCP) + "\n"

			MyString += "#" + str(ilvl + 1) + " GUESSER\n"
			MyString += self.Guesser + "\n"

			MyString += "#" + str(ilvl + 1) + " GUESSINCREMENTER\n"
			MyString += self.GuessInc + "\n"

		elif self.Type == "CPALS":
			MyString += "#" + str(ilvl + 1) + " TYPE\n"
			MyString += self.Type + "\n" 

			MyString += "#" + str(ilvl + 1) + " CPALSTHRESHOLD\n" 
			MyString += "{0:.2e}".format(self.ThrFindCP) + "\n"

			MyString += "#" + str(ilvl + 1) + " CPALSMAXITER\n"
			MyString += str(self.MaxIter) + "\n"

			MyString += "#" + str(ilvl + 1) + " GUESSER\n"
			MyString += self.Guesser + "\n"

		elif self.Type == "FINDBESTCPGRAD":
			MyString += "#" + str(ilvl + 1) + " TYPE\n"
			MyString += self.Type + "\n"

			MyString += "#" + str(ilvl + 1) + " GUESSER\n"
			MyString += self.Guesser + "\n"

		else:
			print "Unknown CP-ALS algorithm: " + self.Type
			sys.exit(0) 

		return MyString
