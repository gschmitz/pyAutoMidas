
Dear everybody,

when I started with Midas I begun to write a set of few script to setup the input
using python for my calculation in order to automatize stuff and to reuse 
a few of my existing bash scripts. For that purpose I also wrote something which 
mimics turbomole's define and interactively sets up the input. 
Currently it only supports the calculation I have done so far, but it is not 
hard to add new functionalities. Maybe it becomes handy for you as well and
concerning new users I'd like to have something which guides through the 
input generation. 

If you want to have a look at it you can clone it from

git@gitlab.com:gschmitz/pyAutoMidas.git

There are also two samples:

1. ecor.inp : Sets up the input for THC-MP2 calculations (very boring for all of you)
2. freq.inp : Sets up the input for a normal mode analysis using turbomole for the
              single point calculations. 

The idea behind the set of scripts is simple. There is one python class which represents
the input file in pyAutoMidas.py and several modules which represents the different
sections in the input file. So far:

modDecompInfo.py       
modModSys.py
modEcor.py
modGeneral.py
modFreqAna.py
modSystem.py
modSinglePoint.py
modSysDef.py

Each contains at least the methods:

GetInteractiveInput()   : Which guides the user to set up the input
GetDataGroup()          : Which generates the input section as string
AddXX(objXX)            : Adds a subsection represented by objXX to this section. In contrast to SetXX a section can have more than one subsection of the kind of objXX.
SetXX(objXX)				: Sets the subsection represented by objXX to this section. In contrast to AddXX a section can only have one subsection of the kind of objXX.

Which define the main interface. So if you want to add something just create a python class with this methods. 

If you want to try the interactive input execute the script

Midasdefine.py

or use the examples

Midasdefine.py < ecor.inp > define.out
Midasdefine.py < freq.inp > define.out

If you want to use it from different folders add the following lines to your .bash_profile

export PYTHONPATH=$PYTHONPATH:<path to pyAutoMidas>
export PATH=<path to pyAutoMidas>:$PATH


What I maybe want to add in the future is that it also reads an existing input and that one can then modify the 
input with the script interactively, but so far it does what I originally wanted it to do.  
Most probably you already have your own set of scripts to automatize the input generation, 
but maybe you are interested in taking a look at it. I like the possibility to generate the input
interactively especially if you are not an experienced user. If you think it is worthwhile
we could extend it to cover at some point the whole functionality of Midas. If not I'm not offended.  

