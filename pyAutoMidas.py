#!/usr/bin/env python

import time

import modEcor
import modGeneral
import modSinglePoint
import modSystem

class MidasInput:

	def __init__(self):
		self.Name = "Midas.inp"

		self.Ecor = modEcor.Ecor()
		self.init_ecor = False

		self.General = modGeneral.General()
		self.init_general = False

		self.SinglePoints = []
		self.init_sp = False
		self.num_sp = 0

		self.System = modSystem.System(self.SinglePoints)
		self.init_system = False

		return	

	def SetEcor(self,objEcor):
		self.Ecor = objEcor
		self.init_ecor = True
		return

	def SetGeneral(self,objGeneral):
		self.General = objGeneral
		self.init_general = True
		return

	def AddSinglePoint(self,objSP):
		# check if we have already a Single Point calculation with the same name
		bfound = False
		for isp in xrange(self.num_sp):
			if self.SinglePoints[isp].Name == objSP.Name:
				bfound = True

		if bfound:
			print " A single point calculation with the same name already exists! I will not add this one!"
		else:
			self.init_sp = True
			self.num_sp += 1
			self.SinglePoints.append(objSP)

		return

	def SetSystem(self,objSystem):
		self.System = objSystem
		self.init_system = True
		return

	def ToFile(self,filename):
		fobj = open(filename, "w")

		fobj.write(" This input file was created with pyAutoMidas 0.1b" + "\n")
		fobj.write(" Date: " + time.strftime("%d/%m/%Y") + " at " + time.strftime("%H:%M:%S") + "\n" )
		fobj.write("\n")

		fobj.write("#0 Midas Input\n")

		# Create info on general setup
		if self.init_general == True:
			fobj.write(self.General.GetDataGroup())

		# create info on single point calculations
		if self.init_sp == True:
			for iloop in xrange(self.num_sp):
				fobj.write(self.SinglePoints[iloop].GetDataGroup())

		# Create info on System 
		if self.init_system == True:
			fobj.write(self.System.GetDataGroup())

		# Create info on correlation energy calculations
		if self.init_ecor == True:
			fobj.write(self.Ecor.GetDataGroup())

		fobj.write("#0 Midas Input End" + "\n")

		fobj.close
		return		
