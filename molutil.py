
import numpy
import string

ang_to_bohr = 1.889725989

dic_mass = {'h'  : 1.00797, 'he' : 4.0026, 'li'  :  6.939,  'be' : 9.0122,  'b'  : 10.811,
            'c'  : 12.01115,'n'  : 14.0067, 'o'  : 15.9994, 'f'  : 18.9984, 'ne' : 20.183,
            'na' : 22.9898, 'mg' : 24.312,  'al' : 26.9815, 'si' : 28.086,  'p'  : 30.9738,
            's'  : 2.064,   'cl' : 35.453,  'ar' : 39.948,  'k'  : 39.102,  'ca' : 40.08,
            'sc' : 44.956,  'ti' : 47.90,   'v'  : 50.942,  'cr' : 51.996,  'mn' : 54.9380,
            'fe' : 55.85,   'co' : 58.9332, 'ni' : 58.71,   'cu' : 63.54,   'zn' : 65.37,
            'ga' : 69.72,   'ge' : 72.59,   'as' : 74.9216, 'se' : 78.96,   'br' : 79.909,
            'kr' : 83.80,   'rb' : 85.47,   'sr' : 87.62,   'y'  : 88.905,  'zr' : 91.22,
            'nb' : 92.906,  'mo' : 95.94,   'tc' : 99.0,    'ru' : 101.07,  'rh' : 102.905,
            'pd' : 106.4,   'ag' : 107.87,  'cd' : 112.40,  'in' : 114.82,  'sn' : 118.69,
            'sb' : 121.75,  'te' : 127.60,  'i'  : 126.904, 'xe' : 131.30,  'cs' : 132.905,
            'ba' : 137.33,  'la' : 138.91,  'ce' : 140.115, 'pr' : 140.908, 'nd' : 144.24,
            'pm' : 146.92,  'sm' : 150.36,  'eu' : 151.965, 'gd' : 157.25,  'tb' : 158.925,
            'dy' : 162.50,  'ho' : 164.93,  'er' : 167.26,  'tm' : 168.93,  'yb' : 173.04,
            'lu' : 174.97,  'hf' : 178.49,  'ta' : 180.95,  'w'  : 183.85,  're' : 186.21,
            'os' : 190.2,   'ir' : 192.22,  'pt' : 195.08,  'au' : 196.97,  'hg' : 200.59,
            'tl' : 204.38,  'pb' : 207.2,   'bi' : 208.980, 'po' : 208.98,  'at' : 209.99,
            'rn' : 222.02,  'fr' : 223.02,  'ra' : 226.03,  'ac' : 227.03,  'th' : 232.04,		
   		   'pa' : 231.04,  'u'  : 238.03,  'np' : 37.0482, 'pu' : 244,     'am' : 243,
            'cm' : 247,     'bk' : 247,     'cf' : 251,     'es' : 252,     'fm' : 257,
            'md' : 258,     'no' : 259,     'lr' : 262}

dic_elec = {'h'  : 1,  'he' : 2,  'li' : 3,  'be' : 4,  'b'  : 5,
            'c'  : 6,  'n'  : 7,  'o'  : 8,  'f'  : 9,  'ne' : 10,
            'na' : 11, 'mg' : 12, 'al' : 13, 'si' : 14, 'p'  : 15,
            's'  : 16, 'cl' : 17, 'ar' : 18, 'k'  : 19, 'ca' : 20,
            'sc' : 21, 'ti' : 22, 'v'  : 23, 'cr' : 24, 'mn' : 25,
            'fe' : 26, 'co' : 27, 'ni' : 28, 'cu' : 29, 'zn' : 30,
            'ga' : 31, 'ge' : 32, 'as' : 33, 'se' : 34, 'br' : 35,
            'kr' : 36, 'rb' : 37, 'sr' : 38, 'y'  : 39, 'zr' : 40,
            'nb' : 41, 'mo' : 42, 'tc' : 43, 'ru' : 44, 'rh' : 45,
            'pd' : 46, 'ag' : 47, 'cd' : 48, 'in' : 49, 'sn' : 50,
            'sb' : 51, 'te' : 52, 'i'  : 53, 'xe' : 54, 'cs' : 55,
            'ba' : 56, 'la' : 57, 'ce' : 58, 'pr' : 59, 'nd' : 60,
            'pm' : 61, 'sm' : 62, 'eu' : 63, 'gd' : 64, 'tb' : 65,
            'dy' : 66, 'ho' : 67, 'er' : 68, 'tm' : 69, 'yb' : 70,
            'lu' : 71, 'hf' : 72, 'ta' : 73, 'w'  : 74, 're' : 75,
            'os' : 76, 'ir' : 77, 'pt' : 78, 'au' : 79, 'hg' : 80,
            'tl' : 81, 'pb' : 82, 'bi' : 83, 'po' : 84, 'at' : 85,
            'rn' : 86, 'fr' : 87, 'ra' : 88, 'ac' : 89, 'th' : 90,
            'pa' : 91,  'u' : 92, 'np' : 93,' pu' : 94, 'am' :  95,
            'cm' : 96, 'bk' : 97, 'cf' : 98, 'es' : 99, 'fm' : 100,
			   'md' : 101,'no' : 102,'lr' : 103}

def read_coord(ffile="coord"):

	xyz = numpy.zeros([0,3],dtype="float64")
	elems = []
	natoms = 0

	fobj = open(ffile, "r")
	iscoord = False

	for line in fobj:
		str1 = line.split() 
       
		if iscoord == True:
			# leave if next datagroup is reached 
			if str1[0][0] == "$":
				break	
           
			coord_info = line.split()
			e = coord_info[3].lower()
			elems.append(e)

			pos = map(float, coord_info[0:3]) 
			xyz = numpy.vstack([xyz,pos])
			natoms += 1
		if str1[0] == "$coord":
			iscoord = True
   
	fobj.close()

	return natoms, xyz, elems

def read_xyz(fname):

	xyz = numpy.zeros([0,3],dtype="float64")
	elems = []
	natoms = 0

	f = open(fname, "r")
	line = string.split(f.readline())
	N = string.atoi(line[0])

	f.readline()   # read title
	for i in xrange(N):
		line = string.split(f.readline())
		e = line[0].lower()
		elems.append(e)

		pos = map(float, line[1:4]) 
		pos = numpy.multiply(ang_to_bohr, pos)

		xyz = numpy.vstack([xyz,pos])
		natoms += 1
	f.close()
	return natoms, xyz, elems
