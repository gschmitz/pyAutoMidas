
import sys
import string
import pySPGen

class SinglePoint:

	def __init__(self):

		self.Name = "sp"
		self.Type = "SP_GENERIC"
		self.InputScript = "Create.sh"
		self.RunScript = "RunScript.sh"
		self.ValidationScript = "ValidationScript.sh"

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for single point calculation"
			print ""
			print "     name <string>  : set the name for this single point calculation"
			print "     type <string>  : set the type of the single point calculation Options:"
			print "                      SP_DALTON  - Calculation with Dalton"
			print "                      SP_CFOUR   - Calculation with CFOUR"
			print "                      SP_ACES    - Calculation with ACES"
			print "                      SP_GENERIC - Calculation with user defines QM program e.g. Turbomole"
			print "                      SP_MODEL   - Use model potential"
			print "                      SP_TINKER  - Calculation with Tinker"
			print ""
			print "     *              : go to next menu"
			print "     q              : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break

			elif sline.startswith("name "):
				token = sline.split()
				self.Name = token[1]
				print ""
				print " Name for this single point calculation is now: " + str(self.Name)
				print ""			
		
			elif sline.startswith("type"):
				token = sline.split()

				if str.upper(token[1]) == "SP_DALTON" or str.upper(token[1]) == "SP_CFOUR"   or \
					str.upper(token[1]) == "SP_ACES"   or str.upper(token[1]) == "SP_GENERIC" or \
					str.upper(token[1]) == "SP_MODEL"  or str.upper(token[1]) == "SP_TINKER":
					
					self.Type = str.upper(token[1])

				if str.upper(token[1]) == "SP_GENERIC":
					print ""
					print " You selected to use a generic SP calculation. For that you need: "
					print "   1. a script, which creates the input for the external program.          Default name: Create.sh"  
					print "   2. a script, which executes the external program and parses the output. Default name: RunScript.sh"
					print "   3. a script, which checks if the external program ended properly.       Default name: ValidationScript.sh"
					print ""
					print " You can select and modify some available Templates."
					print ""

					use_spgen = False
					while True:
						sline = raw_input("use template [Y/N]  ").strip()
						if str.upper(sline) == "Y":
							use_spgen = True
							break
						elif str.upper(sline) == "N":
							use_spgen = False
							break
						else:
							print "Unknwon option. Enter Y or N"
					
					if use_spgen == True:
						objSPGen = pySPGen.SingPointGen()
						objSPGen.CreateInputInteractively()
  
				print ""
				print " Type is now: " + str(self.Type)
				print ""			
		

		return

	def GetDataGroup(self):

		MyString = "#1 SinglePoint\n"

		MyString += "#2 Name\n"
		MyString += self.Name + "\n"

		MyString += "#2 Type\n"
		MyString += self.Type + "\n"

		if self.Type == "SP_GENERIC":
			MyString += "#2 Inputcreatorscript\n"
			MyString += self.InputScript + "\n"

			MyString += "#2 Runscript\n"
			MyString += self.RunScript + "\n"

			MyString += "#2 Validationscript\n"
			MyString += self.ValidationScript + "\n"

		return MyString

