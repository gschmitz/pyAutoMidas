
import sys
import os
import stat

class SingPointGen:

	def __init__(self):

		self.program = "turbomole"
		
		self.basis = "cc-pVDZ"

		self.cbas  = "cc-pVDZ"
		self.init_cbas = False

		self.jkbas = "cc-pVDZ"
		self.init_jkbas = False

		self.method = "rimp2"
		self.useF12 = False

		self.charge = 0

		self.FrozenCore = True

		self.memory = 1500

		return

	def CreateInputInteractively(self):

		#########################################################
		# select external QM program
		#########################################################
		while True:
		
			print ""
			print "        Setup menu for generic Single Point calculation"
			print ""
			print "     program <string>  : sets the used externam program. Default: turbomole. Available:"
			print "                         [1] turbomole"
			print "                         ... further coming soon"
			print ""
			print "     *                 : go to next menu"
			print "     q                 : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break
		
			elif sline.startswith("program "):
				token = sline.split()
				if str.lower(token[1]) == "turbomole":
					self.program = "turbomole"
				else:
					print "Unsupported external program. I do nothing!"

				print ""
				print "Current external program is. " + self.program			
				print ""


		#########################################################
		# select a method
		#########################################################
		while True:
		
			print ""
			print "        Select a method for the single point calculation"
			print ""
			print "     model <string>    : select the computational method. Default: rimp2. Available:"
			print "                         [1] hf      : Hartree-Fock calculation"
			print "                         [2] rimp2   : RI-MP2 calculation"
			print "                         [3] ccsd(t) : CCSD(T) calculation"
			print "                         ... further coming soon"
			print ""
			print "     f12 <on/off>      : switches F12 option on and off. Default is off"
			print "     freeze <on/off>   : switches frozen core option on and off. Default is on"
			print "     *                 : go to next menu"
			print "     q                 : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break
		
			elif sline.startswith("model "):
				token = sline.split()
				if str.lower(token[1]) == "hf" or str.lower(token[1]) == "rimp2" or str.lower(token[1]) == "ccsd(t)":
					self.method = str.lower(token[1]) 
				else:
					print "Unsupported method. I do nothing!"

				print ""
				print "Current method is. " + self.method			
				print ""

			elif sline.startswith("f12 "):
				token = sline.split()
				if str.lower(token[1]) == "on":
					self.useF12 = True
				elif str.lower(token[1]) == "off":
					self.useF12 = False
				else: 	
					print " Unknown option for F12! -> I will change nothing"
				print ""
				print " f12 is now: " + str(self.useF12)
				print ""		

			elif sline.startswith("freeze "):
				token = sline.split()
				if str.lower(token[1]) == "on":
					self.FrozenCore = True
				elif str.lower(token[1]) == "off":
					self.FrozenCore = False
				else: 	
					print " Unknown option for freeze! -> I will change nothing"
				print ""
				print " FrozenCore is now: " + str(self.FrozenCore)
				print ""		



		#########################################################
		# select a basis set
		#########################################################
		while True:
		
			print ""
			print "        Assign basis sets"
			print ""
			print "     bas   <string>    : select the orbital basis set"
			print "     cbas  <string>    : select the auxiliary basis set"
			print "     jkbas <string>    : select the auxiliary JK basis set"
			print "     charge <int>      : set charge mof molecule. Default is 0"
			print "     memory <int>      : set memory in MB. Default 1500"
			print ""
			print "     *                 : go to next menu"
			print "     q                 : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("bas "):
				token = sline.split()
				self.basis = token[1]

				# as long as cbas and jkbas are not set by the user set it to basis
				if not self.init_cbas == True:
					self.cbas = token[1]

				if not self.init_jkbas == True:
					self.jkbas = token[1]

			elif sline.startswith("cbas "):
				token = sline.split()
				self.cbas = token[1]

			elif sline.startswith("jkbas "):
				token = sline.split()
				self.jkbas = token[1]

			elif sline.startswith("charge "):
				token = sline.split()
				self.charge = int(token[1])

			elif sline.startswith("memory "):
				token = sline.split()
				self.memory = int(token[1])

			elif sline.startswith("*"): 
				break
		
		# create Input files
		self.CreateInputScript()
		self.CreateRunScript()
		self.CreateValidateScript()
		self.CreateMidasPropInfo()	
		return

	def CreateMidasPropInfo(self):
		try:
			os.stat("savedir")
		except:
			os.mkdir("savedir")
 
		fobj = open("savedir/midasifc.propinfo", "w")
		fobj.write("tens_order=(0),descriptor=(GROUND_STATE_ENERGY),der_file=(midasifc.my_deriva)")
		fobj.close()

		return

	def CreateRunScript(self):
		try:
			os.stat("savedir")
		except:
			os.mkdir("savedir")
 
		fobj = open("savedir/RunScript.sh", "w")

		fobj.write("#!/bin/bash\n")
		fobj.write("#\n")
		fobj.write("#  Script to run Turbomole calculations for Midas\n")
		fobj.write("#\n")
		fobj.write("#\n")
		fobj.write("#  Arguments:\n")
		fobj.write("#\n")
		fobj.write("#  $1 : The scratch directory\n")
		fobj.write("#  $2 : The done file to be touched upon\n")
		fobj.write("#       successful completion\n")
		fobj.write("#\n\n")

		fobj.write("# not really necessary if it is already in the PATH....\n")
		fobj.write("export TURBODIR=/home/gschmitz/TURBO_WORK\n\n")

		fobj.write("PATH=$PATH:$TURBODIR/scripts\n")
		fobj.write("PATH=$PATH:$TURBODIR/bin/`sysname`\n\n")

		fobj.write("#################################################################\n")
		fobj.write("# function to create property file for Midas\n")
		fobj.write("################################################################\n")
		fobj.write("create_property_file() {\n")
		fobj.write("\n")
		fobj.write("   # Get number of atoms\n")
		fobj.write("   val=$(sdg coord | wc -l)\n")
		fobj.write("   nat=$((val - 1))\n")
		fobj.write("\n")

		fobj.write("   # Get " + self.method + "/" + self.basis + " ground state energy \n")
		if self.method == "rimp2":
			fobj.write("   energy=$(grep 'Total Energy' TURBO.OUT | awk '{print $4}')\n")
			
		elif self.method == "ccsd(t)":
			if self.useF12:
				fobj.write("   energy=$(grep 'Final CCSD(F12\*)(T) energy' TURBO.OUT | awk '{print $6}')\n")
			else:
				fobj.write("   energy=$(grep 'Final CCSD(T) energy' TURBO.OUT | awk '{print $6}')\n")

		fobj.write("\n")

		fobj.write('   echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general\n')
		fobj.write("\n")

		fobj.write("   # Get gradient\n")
		fobj.write("   echo $nat > midasifc.my_deriva\n")
		sexpr = repr("   tail -n$val gradient | head -n$nat | sed -e 's/ /\n/g' | grep [0-9] | sed 's/D/E/g' >> midasifc.my_deriva")[1:]
		str1 = sexpr[:-1]
		fobj.write(str1 + "\n")
		fobj.write("}\n")
		fobj.write("\n")

		fobj.write("# Change dir to $1 (the scratch dir)\n")
		fobj.write("cd $1\n")
		fobj.write("\n")

		fobj.write("# All files are here, execute Turbomole commands...\n")
		fobj.write("dscf &> TURBO.OUT\n")
		if self.method == "rimp2":
			fobj.write("ricc2 &>> TURBO.OUT\n")
		elif self.method == "ccsd(t)":
			fobj.write("ccsdf12 &>> TURBO.OUT\n")
			fobj.write("NumGrad &>> TURBO.OUT\n")

		fobj.write("\n")

		fobj.write("# After completion, setup file with properties\n")
		fobj.write("# according to midasifc.prop_info located in savedir\n")
		fobj.write("create_property_file\n")
		fobj.write("\n")
		fobj.write("# And finally we touch the done file\n")
		fobj.write("# to let MidasCpp know we are done!\n")
		fobj.write("touch $2\n")

		fobj.close()

		st = os.stat("savedir/RunScript.sh")
		os.chmod("savedir/RunScript.sh", st.st_mode | stat.S_IEXEC)
		return

	def CreateInputScript(self):
		try:
			os.stat("savedir")
		except:
			os.mkdir("savedir")
 
		fobj = open("savedir/Create.sh", "w")
	
		fobj.write("#!/bin/bash\n")
		fobj.write("#\n")
		fobj.write("#  " + self.method + "/" + self.basis + " calculation\n")
		fobj.write("#\n\n")

		fobj.write("export TURBODIR=/home/gschmitz/progs/TURBOMOLE/TM-V6.5\n") # ToDo set TM Path
		fobj.write("PATH=$PATH:$TURBODIR/scripts\n")
		fobj.write("PATH=$PATH:$TURBODIR/bin/`sysname`\n\n")

		fobj.write("# function for creating a define input\n")
		fobj.write("setup_define() {\n")
		fobj.write("cat << %EOF% > define.inp\n\n")
		
		fobj.write("TURBOMOLE CALCULATION FOR MIDAS\n")
		fobj.write("a coord\n")
		fobj.write("*\n")
		fobj.write("no\n")
		fobj.write("bb all " + self.basis + "\n")
		fobj.write("*\n")
		fobj.write("eht\n")
		fobj.write("y\n")
		fobj.write(str(self.charge) + "\n")
		fobj.write("y\n")

		if self.method == "rimp2" or self.method == "ccsd(t)":
			fobj.write("cc\n")
		
			if self.FrozenCore == True:
				fobj.write("freeze\n") 
				fobj.write("*\n")

			fobj.write("cbas\n")
			if self.basis == self.cbas:
				fobj.write("*\n")
			else:
				fobj.write("bb all " + self.cbas + "\n")
				fobj.write("*\n")

			if self.useF12:
				fobj.write("f12\n")
				fobj.write("*\n")
				fobj.write("cabs\n")
				fobj.write("*\n")

			fobj.write("ricc2\n")
			if self.method == "rimp2":
				fobj.write("model mp2\n")
				fobj.write("geoopt mp2\n")
			elif self.method == "ccsd(t)":
				fobj.write("ccsd(t)\n")
			fobj.write("oconv 12\n")
			fobj.write("maxiter 200\n")
			fobj.write("*\n")

		fobj.write("memory\n")
		fobj.write(str(self.memory) + "\n")
		fobj.write("*\n")
		fobj.write("*\n")

		fobj.write("%EOF%\n")
		fobj.write("}\n\n")


		fobj.write("# Create coord file from midasifc.xyz_input\n")
		fobj.write("x2t midasifc.xyz_input > coord\n\n")

		fobj.write("# Create define input\n")
		fobj.write("setup_define\n\n")

		fobj.write("# Call define\n")
		fobj.write("define < define.inp > define.out\n\n")

		# add numerical gradient for CCSD(T)
		if self.method == "ccsd(t)":	
			fobj.write("# update control file...\n")
			if self.useF12 == True:
				fobj.write("sed -i s/'ccsd(f12)'/'ccsd(f12*)'/g control\n\n")

			fobj.write("# add datagroup numgrad\n")
			fobj.write("kdg end\n")
			fobj.write("kdg laplace\n")
			fobj.write("kdg profile\n")
			fobj.write("kdg scfconv\n")
			fobj.write("kdg denconv\n")
			fobj.write("\n")
			fobj.write('echo "\$scfconv 10" >> control\n')
			fobj.write('echo "\$denconv 0.1000000d-9" >> control\n')
			fobj.write("\n")
			fobj.write('echo "\$numgrad" >> control\n')
			fobj.write('echo "  level ccsdf12" >> control\n')
			fobj.write('echo "  delta 0.001" >> control\n')
			fobj.write('echo "  symad" >> control\n')
			fobj.write("\n")
			fobj.write("\n")
			fobj.write('echo "\$end" >> control\n\n')


		fobj.write("# The coord file can now be copied to\n")
		fobj.write("# midasifc.cartrot_xyz for symmetry module\n\n")

		fobj.write("t2x -c coord > midasifc.cartrot_xyz\n")


		fobj.close()

		st = os.stat("savedir/Create.sh")
		os.chmod("savedir/Create.sh", st.st_mode | stat.S_IEXEC)
		return

	def CreateValidateScript(self):

		try:
			os.stat("savedir")
		except:
			os.mkdir("savedir") 

		fobj = open("savedir/ValidationScript.sh", "w")

		fobj.write("#!/usr/bin/env bash\n\n")

		fobj.write("#****************************************************************#\n") 
		fobj.write("# specific validation definition for our TURBOMOLE calculation   #\n")
		fobj.write("#                                                                #\n")
		fobj.write("# User must provide                                              #\n")
		fobj.write("#  outfilefile         : outputfile from ES program              #\n")
		fobj.write("#  error_check         : array of lines to grep for in output    #\n")
		fobj.write("#                        that if not there will produce an error #\n")	
		fobj.write("#  error_check_val     : array with the number of times the      #\n")
		fobj.write("#                        given line is presumed to be in output  #\n")
		fobj.write("#  suspicious_check    : array of line to grep for in output     #\n")
		fobj.write("#                        that if not there indicates suspicious  #\n")
		fobj.write("#                        behaviour that is not nescesaarily      #\n")
		fobj.write("#                        an error (e.g. not converged)           #\n")
		fobj.write("#  suspicious_check_val: array with the number of times the      #\n")
		fobj.write("#                        given line is presumed to be in output  #\n")
		fobj.write("#  files_to_copy_on_error     : files to copy back on error      #\n")
		fobj.write("#  files_to_copy_if_suspicious: copy back if suspicious          #\n")
		fobj.write("#****************************************************************#\n")
		fobj.write("validate_define(){\n")
		fobj.write("#***************************#\n") 
		fobj.write("# !!! define validation !!! #\n")
		fobj.write("#***************************#\n")
		fobj.write("# outputfile from es program\n")
		fobj.write("   outputfile=TURBO.OUT\n")
		fobj.write("\n")
		fobj.write("# define error checks\n")

		if self.method == "rimp2":
			fobj.write('   error_check[1]="ricc2 : all done"; error_check_val[1]=1\n') 
		elif self.method == "ccsd(t)":
			fobj.write('   error_check[1]="ccsdf12 : all done"; error_check_val[1]=1\n') 

		fobj.write("   files_to_copy_on_error=$outputfile\n")
		fobj.write("\n")
		fobj.write("# define suspicious behavior checks\n")
		fobj.write('   suspicious_check[1]="convergence criteria satisfied after"; suspicious_check_val[1]="1"\n')
		fobj.write("   files_to_copy_if_suspicious=$outputfile\n")
		fobj.write("}\n")
		fobj.write("#****************************************************************#\n")
		fobj.write('# validate function that validates with "grep" and "wl -l"       #\n')
		fobj.write("# commands. What to grep for and number of lines is provided     #\n")
		fobj.write("# by user in define function.                                    #\n")
		fobj.write("#                                                                #\n")
		fobj.write("# On return must                                                 #\n")
		fobj.write("#  save number of errors in:               num_error             #\n")
		fobj.write("#  save number of suspicious behaviors in: num_suspicious        #\n")
		fobj.write("#  put files to copy back in:              files_to_copy         #\n")
		fobj.write("#****************************************************************#\n")
		fobj.write("validate(){\n")
		fobj.write("#*****************************#\n")
		fobj.write("# run define provided by user #\n")
		fobj.write("#*****************************#\n")
		fobj.write("   validate_define\n")	
		fobj.write("\n")
		fobj.write("#********************************************************************#\n")
		fobj.write("# check that input to validation is given correctly (automated part) #\n")
		fobj.write("#********************************************************************#\n")
		fobj.write("# check output file is there\n")
		fobj.write("   if [ ! -s $outputfile ]; then\n")
		fobj.write('      echo "file $outputfile does not exist or is empty"\n')
		fobj.write("      exit 40\n")
		fobj.write("   fi\n")
		fobj.write("\n")
		fobj.write("# check checking parameters\n")
		fobj.write("   if [ ${#error_check[@]} -ne ${#error_check_val[@]} ]; then\n")
		fobj.write('      echo "size of error_check and error_check_val are different"\n')
		fobj.write("      exit 40\n")
		fobj.write("   fi\n")
		fobj.write("   if [ ${#suspicious_check[@]} -ne ${#suspicious_check_val[@]} ]; then\n")
		fobj.write('      echo "size of suspicious_check and suspicious_check_val are different"\n')
		fobj.write("      exit 40\n")
		fobj.write("   fi\n")
		fobj.write("\n")
		fobj.write("#********************************#\n")
		fobj.write("# do validation (automated part) #\n")
		fobj.write("#********************************#\n")
		fobj.write("# Make error checks\n")
		fobj.write("   for ((i=1; i<=${#error_check[@]}; i++)); do\n")
		fobj.write('      check=`grep "${error_check[$i]}" $outputfile | wc -l`\n')	
		fobj.write("      if [ $check -ne ${error_check_val[$i]} ]; then\n")
		fobj.write("         ((num_error++))\n")
		fobj.write("      fi\n")
		fobj.write("   done\n")
		fobj.write("\n")
		fobj.write("# Make suspicious checks\n")
		fobj.write("   for ((i=1; i<=${#suspicious_check[@]}; i++)); do\n")
		fobj.write('      check=`grep "${suspicious_check[$i]}" $outputfile | wc -l`\n')
		fobj.write("      if [ $check -ne ${suspicious_check_val[$i]} ]; then\n")
		fobj.write("         ((num_suspicious++))\n")
		fobj.write("      fi\n")
		fobj.write("   done\n")
		fobj.write("\n")
		fobj.write("# TODO: make some output on fail checks !\n")
		fobj.write("\n")
		fobj.write("# copy back files?\n")
		fobj.write('   if [ $num_error -ne 0 ]; then files_to_copy=$files_to_copy" "$files_to_copy_on_error; fi\n')
		fobj.write('   if [ $num_suspicious -ne 0 ]; then files_to_copy=$files_to_copy" "$files_to_copy_if_suspicious; fi\n')
		fobj.write("}\n")
		fobj.write("\n")
		fobj.write("#*************************************#\n")
		fobj.write("# generel validation wrapper function #\n")
		fobj.write("#*************************************#\n")
		fobj.write("validate_wrapper() {\n")
		fobj.write("# initialize number of errors and suspicious things\n")
		fobj.write("   num_error=0\n")
		fobj.write("   num_suspicious=0\n")
		fobj.write("   files_to_copy=\n")
		fobj.write("\n")
		fobj.write("# run validation for specific calculation\n")
		fobj.write("   validate\n")
		fobj.write("\n")
		fobj.write("# make error file\n")
		fobj.write("   echo $num_error\n") 
		fobj.write("   echo $num_suspicious \n")
		fobj.write("   echo $files_to_copy \n")
		fobj.write("}\n")
		fobj.write("#*******************************#\n")
		fobj.write("# Run validation script wrapper #\n")
		fobj.write("#*******************************#\n")
		fobj.write("validate_wrapper\n")
	
		fobj.close()

		st = os.stat("savedir/ValidationScript.sh")
		os.chmod("savedir/ValidationScript.sh", st.st_mode | stat.S_IEXEC)
		return

