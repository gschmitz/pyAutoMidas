
import sys

class General:

	def __init__(self):

		self.IoLevel = 10

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for general input"
			print ""
			print "     iolevel <int>  : set global IoLevel"
			print "     *              : go to next menu"
			print "     q              : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break
		
			elif sline.startswith("iolevel "):
				token = sline.split()
				self.IoLevel = int(token[1])
				print ""
				print " IoLevel is now: " + str(self.IoLevel)
				print ""			

		return

	def GetDataGroup(self):

		MyString = "#1 General\n"

		MyString += "#2 IoLevel\n"
		MyString += str(self.IoLevel) + "\n"

		return MyString



