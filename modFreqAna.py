
import sys

class FreqAna:

	def __init__(self,ilevel,lstOfSinglePoints):

		self.InputLevel = ilevel

		self.NumNodes = 1 

		self.ProcPerNode = 1

		self.SinglePoints = lstOfSinglePoints
		self.SinglePoint_idx = 0

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for general input"
			print ""
			print "     nodes <int>    : Specify the number of compute nodes. Default is 1" 
			print "     proc  <int>    : Specify the number of processes per nodes. Default is 1"
			print "     selsp          : Select a single point calculation"
			print "     *              : go to next menu"
			print "     q              : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break

			elif sline.startswith("nodes "):
				token = sline.split()
				self.NumNodes = int(token[1])

			elif sline.startswith("proc "):
				token = sline.split()
				self.ProcPerNode = int(token[1])

			elif sline.startswith("selsp"):
				print ""
				print " Following Single Point Calculations are available:"

				isp = 1
				for objSP in self.SinglePoints:
					print "[1] : " + objSP.Name + "\n"
					isp += 1

				print ""
				sline = raw_input("select one:  ").strip()

				idx = int(sline) - 1
				if idx >= 0 and idx < isp:
					self.SinglePoint_idx = idx
				else:
					print " No proper index selected! I will take the first one"		
	 
		return

	def GetDataGroup(self):

		#shortcut 
		ilvl = self.InputLevel

		MyString = "#" + str(ilvl) + " SinglePointName\n"

		if not len(self.SinglePoints) == 0:
			MyString += self.SinglePoints[self.SinglePoint_idx].Name + "\n"
		else:
			MyString += "<enter name of SP>"

		MyString += "#" + str(ilvl) + " NumNodes\n"
		MyString += str(self.NumNodes) + "\n"

		MyString += "#" + str(ilvl) + " ProcPerNode\n"
		MyString += str(self.ProcPerNode) + "\n"

		return MyString


