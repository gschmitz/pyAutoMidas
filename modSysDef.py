import numpy

import os
import os.path
import sys
import molutil

class SysDef:

	def __init__(self,ilevel):

		self.Name = "molecule"

		self.InputLevel = ilevel

		self.MolFileType = "Midas"
		self.MolFile = "Molecule.mol"

		self.CalcModes = "Cart"

		self.xyz = numpy.zeros([0,3],dtype="float64")
		self.xyz_init = False
		self.elems = []
		self.natoms = 0
		self.nelect = 0

		return

	def GetInteractiveInput(self):
		while True:
		
			print ""
			print "        Setup menu for (Sub)System definitions"
			print ""
			print "     a <file>       : add molecule file. Supported:"
			print "                      [1] Midas .mol files"
			print "                      [2] .xyz files"
			print "                      [3] Turbomole coord files"
			print ""
			print "     name <string>  : set the name for this (sub)system"
			print "     *              : go to next menu"
			print "     q              : quit progam (input will not be written)"
			print ""
		
			sline = raw_input("Command:  ").strip()
		
			if sline.startswith("q"): 
				sys.exit(0)
		
			elif sline.startswith("*"): 
				break

			elif sline.startswith("a "):
				token = sline.split()
				self.SetMolFile(token[1])

			elif sline.startswith("name "):
				token = sline.split()
				self.Name = token[1]
				print ""
				print " Name for this single point calculation is now: " + str(self.Name)
				print ""			
		

		return

	def SetMolFile(self,path):

		if os.path.isfile(path) == True:
			extension = os.path.splitext(path)[1]

			if extension == ".mol":
				print "Warning: Currently it is not checked for .mol files if this is a valid file!"
				print "         It will only be checked if it exists"

				self.MolFile = path
				self.MolFileType = "Midas"

			elif extension == ".xyz":
				self.natoms, self.xyz, self.elems = molutil.read_xyz(path)
				self.WriteMolFile("Molecule.mol")
			
			elif os.path.basename(path) == "coord":
				self.natoms, self.xyz, self.elems = molutil.read_coord(path)
				self.WriteMolFile("Molecule.mol")

			else:
				print "Warning: Unknown file format"
				self.MolFile = "<please enter .mol file>"
		else:
			print "Warning: File not found"
			self.MolFile = "<please enter .mol file>"

		return

	def WriteMolFile(self,molfil):
		fobj = open(molfil, "w")

		fobj.write("#0MOLECULEINPUT\n\n")

		fobj.write("#1XYZ\n")
		fobj.write(str(self.natoms) + " au\n")
		fobj.write("Comment line\n")
		for i in xrange(self.natoms):
			xyz = self.xyz[i]
			sline = "%-5s %12.6f %12.6f %12.6f" % tuple([self.elems[i]]+xyz.tolist())
			fobj.write(sline + "\n") 

		fobj.write("\n")
		fobj.write("#0MOLECULEINPUTEND")
		return


	def GetDataGroup(self):

		# shortcut
		ilvl = self.InputLevel

		MyString = "#" + str(ilvl) + " SysDef\n"

		MyString += "#" + str(ilvl + 1) + " Moleculefile\n"
		MyString += self.MolFileType + "\n"
		MyString += self.MolFile + "\n"

		MyString += "#" + str(ilvl + 1) + " CalcModes\n"
		MyString += self.CalcModes + "\n"

		MyString += "#" + str(ilvl + 1) + " Name\n"
		MyString += self.Name + "\n"

		return MyString



